const log4js = require('log4js')

module.exports = {
    init: function (propConfig) {
        log4js.configure({
            appenders: {
                file: {
                    type: 'file',
                    layout: {
                        type: 'pattern',
                        pattern: '%d{yyyy-MM-dd hh:mm:ss} [%z] %p %m%n'
                    },
                    filename: propConfig.logger.file,
                    maxLogSize : 10485760,
                    backups : 3,
                    compress : true
                }
            },
            categories: {
                default: {
                    appenders: ['file'],
                    level: propConfig.logger.level
                }
            },
        }
        )
    },
    file: log4js.getLogger('file')
}