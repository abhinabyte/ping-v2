const Mongoose = require("mongoose")

const logger = require('../config/logConfig').file

module.exports = {
    init: async function (propConfig) {
        global.Promise = require("bluebird")
        
        const options = {
            server: {
                useNewUrlParser: true,
                useMongoClient: true,
                user: propConfig.mongo.username,
                password: propConfig.mongo.password,
                autoIndex: false,
                promiseLibrary: global.Promise,
                reconnectTries: Number.MAX_VALUE,
                reconnectInterval: 5000,
                bufferMaxEntries: 0,
                poolSize: 10,
                connectTimeoutMS: 10000,
                socketTimeoutMS: 25000
            }
        }

        Mongoose.connect(propConfig.mongo.url, options).then(() => {
            logger.info('Connected to MongoDB at ', propConfig.mongo.url)
            return Mongoose.connection
        }).catch(err =>logger.error(err))
    }
}