const os = require('os')
const cluster = require('cluster')

module.exports = {
    init: function (app, propConfig) {
        if (cluster.isMaster) {
            for (var i = 0; i < os.cpus().length; i++) {
                cluster.fork();
            }

            app.listen(propConfig.port, (e) => {
                if (e) {
                    return console.error(e)
                }
                console.log(`Server listening to port: ${propConfig.port}`)
            })
            cluster.on('exit', (worker, code, signal) => {
                console.log(`worker ${worker.process.pid} died`)
            })
        }

    }
}