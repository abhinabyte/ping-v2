const AWS = require('aws-sdk')
const uuid = require('uuid/v4')

var s3 = {}

module.exports = {
    init: (propConfig) => {
        AWS.config.update({ accessKeyId: propConfig.aws.accessKeyId, secretAccessKey: propConfig.aws.secretAccessKey })
        AWS.config.setPromisesDependency(global.Promise)
        s3 = new AWS.S3()
    },
    getS3: () => {
        return s3
    }
}
