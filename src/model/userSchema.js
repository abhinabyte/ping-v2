const mongoose = require("mongoose")

mongoose.Promise = global.Promise

const Schema = mongoose.Schema

const userSchema = new Schema({
    id: { type: String, index: true, unique: true },
    name: {
        first: { type: String, required: true },
        middle: { type: String },
        last: { type: String, required: true },
        nick_name: { type: String, required: true }
    },
    tagline: { type: String, required: true, index: true, unique: true },
    profile_pic: {
        key: { type: String },
        type: { type: String }
    },
    cover_pic: {
        key: { type: String },
        type: { type: String }
    },
    birthday: { type: Date },
    address: {
        city: { type: String },
        country: { type: String }
    },
    education: { type: String },
    marital_status: { type: String },
    rating: {
        normal: { type: Number },
        sos: { type: Number }
    },
    awards: {

    },
    visibility: { type: String }
}, { versionKey: false })

module.exports = mongoose.model("users", userSchema, 'users')