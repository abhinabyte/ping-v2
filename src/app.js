const Koa = require('koa')
const bodyParser = require('koa-bodyparser')

const propConfig = require('./config/propConfig')[process.env.NODE_ENV || "local"]
const logConfig = require('./config/logConfig')
const mongoConfig = require('./config/mongoConfig')
const awsConfig = require('./config/awsConfig')
const serverConfig = require('./config/serverConfig')
const router = require('./routes')

const app = new Koa()

app.use(bodyParser()).use(router.routes()).use(router.allowedMethods())

logConfig.init(propConfig)
serverConfig.init(app, propConfig)
mongoConfig.init(propConfig)
awsConfig.init(propConfig)
