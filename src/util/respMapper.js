module.exports = {
    success: async (data, ctx) => {
        let resp = {}
        resp.success = true
        resp.data = data
        ctx.body = resp
    },
    failure : async (code, desc, ctx) => {
        let resp = {}
        resp.success = false
        ctx.status = 400
        resp.code = code
        resp.desc = desc
        ctx.body = resp
    },
    noContent : async ctx => {
        let resp = {}
        resp.success = false
        ctx.status = 204
        ctx.body = resp
    }
}
