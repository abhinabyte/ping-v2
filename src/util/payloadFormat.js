var payloadFormat = {}

payloadFormat.userInfo = {
    "type": "object",
    "properties": {
        "id": { "type": "string" },
        "name": {
            "type": "object",
            "properties": {
                "first": { "type": "string" },
                "middle": { "type": "string" },
                "last": { "type": "string" },
                "nick_name": { "type": "string" }
            },
            "required": ["first", "last", "nick_name"]
        },
        "tagline": { "type": "string" },
        "profile_pic": {
            "type": "object",
            "properties": {
                "data": { "type": "string", "required": "true" },
                "type": { "type": "string", "required": "true" }
            }
        },
        "cover_pic": {
            "type": "object",
            "properties": {
                "data": { "type": "string", "required": "true" },
                "type": { "type": "string", "required": "true" }
            }
        },
        "birthday": { "type": "Date" },
        "address": {
            "city": { "type": "string" },
            "country": { "type": "string" }
        },
        "education": { "type": "string" },
        "marital_status": { "type": "string" },
        "rating": {
            "normal": { "type": "number" },
            "sos": { "type": "number" }
        },
        "visibility": { "type": "string", "enum": ['PRIVATE', 'PUBLIC'] }
    },
    "required": ["id", "name", "tagline"]
}

module.exports = payloadFormat
