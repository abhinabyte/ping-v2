const uuid = require('uuid/v4')

const propConfig = require('../config/propConfig')[process.env.NODE_ENV || "local"]
const awsConfig = require('../config/awsConfig')

module.exports = {
    upload: (data, type) => {
        const s3 = awsConfig.getS3
        const key = uuid() + '.' + type
        const params = {
            Bucket: propConfig.aws.bucket,
            Key: key,
            Body: data,
            ContentEncoding: 'base64'
        }
        return new Promise((resolve, reject) => {
            try {
                s3.upload(params, (err, data) => {
                    if (err) { return reject(err) }
                    resolve(key)
                })
            } catch (err) {
                return reject(err)
            }
        })
    }
}