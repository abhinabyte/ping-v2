const userSchema = require('../model/userSchema')

module.exports = {
    save: (userInfo) => {
        return new Promise((resolve, reject) => {
            try {
                let user = new userSchema(userInfo)
                user.save((err) => {
                    if (err) {
                        return reject(err)
                    } else {
                        resolve(user)
                    }
                })
            } catch (err) {
                return reject(err)
            }
        })
    },
    findOneAndUpdate: (query, userInfo) => {
        return new Promise((resolve, reject) => {
            try {
                let data = {}
                if (userInfo.name) {
                    let name = {}

                    name.first = userInfo.name.first
                    name.middle = userInfo.name.middle
                    name.last = userInfo.name.last
                    name.nick_name = userInfo.name.nick_name

                    data.name = name
                }

                if (userInfo.tagline) {
                    data.tagline = userInfo.tagline
                }

                if (userInfo.birthday) {
                    data.birthday = userInfo.birthday
                }

                if (userInfo.address) {
                    let address = {}

                    address.city = userInfo.address.city
                    address.country = userInfo.address.country

                    data.address = address
                }

                if (userInfo.education) {
                    data.education = userInfo.education
                }

                if (userInfo.marital_status) {
                    data.marital_status = userInfo.marital_status
                }

                if (userInfo.rating) {
                    let rating = {}

                    rating.normal = userInfo.rating.normal
                    rating.sos = userInfo.rating.sos

                    data.rating = rating
                }

                if (userInfo.visibility) {
                    data.visibility = userInfo.visibility
                }

                userSchema.findOneAndUpdate(query, data, { new: false, upsert: true }, (err, user) => {
                    if (err || !user) {
                        return reject(err)
                    } else {
                        resolve(data)
                    }
                })
            } catch (err) {
                return reject(err)
            }
        })
    },
    findOne: (query) => {
        return new Promise((resolve, reject) => {
            try {
                userSchema.findOne(query, (err, user) => {
                    if (err || !user) {
                        return reject(err)
                    } else {
                        resolve(user)
                    }
                })
            } catch (err) {
                return reject(err)
            }
        })
    }
}