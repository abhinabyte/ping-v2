const Router = require('koa-router')
const userCtrl = require('./controller/userCtrl')
const router = new Router()

router.post('/users/:id/v1/save', userCtrl.save)
router.post('/users/:id/v1/update', userCtrl.update)

module.exports = router