const schemaValidator = require('jsonschema').Validator
const logger = require('../config/logConfig').file
const payloadFormat = require('../util/payloadFormat')
const respMapper = require('../util/respMapper')
const userService = require('../service/userService')

var userCtrl = {}

var validator = new schemaValidator()

userCtrl.save = async ctx => {
    let userInfo = ctx.request.body

    userInfo.id = ctx.params.id

    if (validator.validate(userInfo, payloadFormat.userInfo).valid) {
        await userService.save(userInfo).then(user => {
            respMapper.success(user, ctx)
        }, err => {
            logger.error('Unable to save user details', err)
            respMapper.failure('1002', 'Unable to save the user details', ctx)
        })
    } else {
        respMapper.failure('1001', 'Invalid payload detected', ctx)
    }
}

userCtrl.update = async ctx => {
    let userInfo = ctx.request.body

    userInfo.id = ctx.params.id

    await userService.findOneAndUpdate({ id: ctx.params.id }, userInfo).then(status => {
        respMapper.success(status, ctx)
    }, err => {
        logger.error('Unable to update user details', err)
        respMapper.failure('1003', 'Unable to update the user details', ctx)
    })
}

module.exports = userCtrl
